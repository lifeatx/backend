#!/bin/bash

for CMD in `ls cmd`; do
  GOOS=linux GOARCH=amd64 go build -o ./bin/$CMD ./cmd/$CMD
done

GOOS=linux GOARCH=amd64 go build -o ./bin/application

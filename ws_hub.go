package main

import (
	"backend/models"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/labstack/echo"
)

type WSHub struct {
	clients   map[*websocket.Conn]bool
	broadcast chan models.Post
	upgrader  websocket.Upgrader
}

func NewWSHub(broadcast chan models.Post) WSHub {
	return WSHub{
		clients:   make(map[*websocket.Conn]bool),
		broadcast: broadcast,
		upgrader:  websocket.Upgrader{},
	}
}

func (h *WSHub) handleConnections(c echo.Context) (err error) {
	h.upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	ws, err := h.upgrader.Upgrade(c.Response(), c.Request(), nil)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	defer ws.Close()

	c.Logger().Info("WS Client has connected")
	h.clients[ws] = true

	for {
		// Read messages
		_, _, err := ws.ReadMessage()
		if err != nil {
			c.Logger().Error(err)
			return err
		}
		c.Logger().Info("WS Client has sent a message and is being ignored")
	}

	return
}

func (h *WSHub) handleMessages() {
	for {
		// Grab the next message from the broadcast channel
		msg := <-h.broadcast

		// Send it out to every client that is currently connected
		for client := range h.clients {
			err := client.WriteJSON(msg)
			if err != nil {
				log.Printf("error: %v", err)
				client.Close()
				delete(h.clients, client)
			}
		}
	}
}

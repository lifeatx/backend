package main

import (
	"net/http"

	jwtmiddleware "github.com/auth0/go-jwt-middleware"
	"github.com/labstack/echo"
)

type Authorizer struct {
	Middleware *jwtmiddleware.JWTMiddleware
}

func NewAuthorizer() *Authorizer {
	jwtAuth := &JWTAuth{}
	jwtMiddleware := jwtAuth.JWTMiddleware()

	return &Authorizer{
		Middleware: jwtMiddleware,
	}
}

func (a *Authorizer) Authorize(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		err := a.Middleware.CheckJWT(c.Response(), c.Request())
		if err != nil {
			return echo.NewHTTPError(http.StatusUnauthorized, "Please provide valid credentials")
		}

		return next(c)
	}
}

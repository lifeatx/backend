package main

import (
	"backend/models"
	"fmt"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/imroc/req"
	"github.com/labstack/echo"
)

func AuthenticateUser(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		authHeader := c.Request().Header.Get("Authorization")
		requestJwt := strings.Split(authHeader, " ")[1]

		ctxUser := c.Request().Context().Value("user")
		claims := ctxUser.(*jwt.Token).Claims.(jwt.MapClaims)
		if claims["sub"] == nil {
			return c.JSON(http.StatusUnauthorized, "Please provide valid credentials")
		}

		var user models.User

		if DB.gorm.Where("sub = ?", claims["sub"]).First(&user).RecordNotFound() {
			err := createUser(&user, requestJwt)
			if err != nil {
				fmt.Println(err)
				return c.JSON(http.StatusInternalServerError, "Could not create user")
			}
		}

		c.Set("User", &user)

		return next(c)
	}
}

type Auth0Response struct {
	Sub      string `json:"sub"`
	Name     string `json:"name"`
	Nickname string `json:"nickname"`
	Picture  string `json:"picture"`
}

func createUser(user *models.User, requestJwt string) error {
	endpoint := "https://dev-edsd1yx0.auth0.com/userinfo"

	header := req.Header{
		"Accept":        "application/json",
		"Authorization": fmt.Sprintf("Bearer %s", requestJwt),
	}

	response, err := req.Get(endpoint, header)
	if err != nil {
		return err
	}

	var profile Auth0Response
	response.ToJSON(&profile)

	user.Email = profile.Name
	user.Sub = profile.Sub

	err = DB.gorm.Create(&user).Error
	if err != nil {
		return err
	}

	return nil
}

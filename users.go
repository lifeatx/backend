package main

import (
	"backend/models"
	"net/http"

	"github.com/labstack/echo"
)

type UserHandlers struct{}

func (u *UserHandlers) GetCurrentUser(c echo.Context) error {
	user := c.Get("User").(*models.User)

	return c.JSON(http.StatusOK, &user)
}

package main

import (
	"backend/models"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/facebookgo/grace/gracehttp"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

const (
	S3_REGION = "us-west-1"
)

var DB *Database

func main() {
	e := echo.New()
	e.Debug = true

	dbURL := os.Getenv("DATABASE_URL")
	if dbURL == "" {
		dbURL = "host=localhost port=5432 user=postgres dbname=lifeatx_dev sslmode=disable"
	}

	// DB Connection
	db, err := gorm.Open("postgres", dbURL)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// AWS Connection
	session, err := session.NewSession(&aws.Config{Region: aws.String(S3_REGION)})
	if err != nil {
		log.Fatal(err)
	}

	// Required objects initialization
	DB = &Database{gorm: db}
	auth := NewAuthorizer()

	broadcast := make(chan models.Post)
	wsHub := NewWSHub(broadcast)

	postHandlers := &PostHandlers{s3Session: session, broadcast: broadcast}
	userHandlers := &UserHandlers{}

	go wsHub.handleMessages()

	// Request Middlewares
	e.Use(auth.Authorize)
	e.Use(AuthenticateUser)
	e.Use(middleware.Logger())

	// Request handlers
	e.GET("/", func(c echo.Context) error {
		user := c.Get("User").(*models.User)
		return c.String(http.StatusOK, fmt.Sprintf("Hello, %s!", user.Email))
	})

	e.GET("/health", func(c echo.Context) error {
		return c.JSON(http.StatusOK, "OK")
	})

	e.GET("/posts", postHandlers.GetPosts)
	e.POST("/posts", postHandlers.UploadPosts)
	e.GET("/user/posts", postHandlers.GetUserPosts)
	e.GET("/user", userHandlers.GetCurrentUser)
	e.GET("/ws", wsHub.handleConnections)

	port := os.Getenv("PORT")
	if port == "" {
		port = "5000"
	}

	e.Server.Addr = fmt.Sprintf(":%s", port)

	e.Logger.Fatal(gracehttp.Serve(e.Server))
}

package models

type User struct {
	Base
	Sub   string `gorm:"not null" json:"sub"`
	Email string `gorm:"not null" json:"email"`
	Posts []Post
}

package models

type Post struct {
	Base
	Content   string `gorm:"not null" json:"content"`
	ImagePath string `gorm:"not null" json:"image_path"`
	UserID    uint   `json:"user_id"`
}

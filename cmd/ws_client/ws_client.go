package main

import (
	"backend/models"
	"flag"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"

	"github.com/gorilla/websocket"
)

var addr = flag.String("addr", "localhost:4000", "http service address")

func main() {
	flag.Parse()
	log.SetFlags(0)

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	u := url.URL{Scheme: "ws", Host: *addr, Path: "/ws"}
	log.Printf("connecting to %s", u.String())

	h := http.Header{}
	h.Add("Authorization", "<Replace with valid bearer token")

	c, resp, err := websocket.DefaultDialer.Dial(u.String(), h)
	if err == websocket.ErrBadHandshake {
		log.Printf("handshake failed with status %d %+v", resp.StatusCode, resp.Body)
	}
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()

	go func() {
		for {
			var post models.Post
			err := c.ReadJSON(&post)
			if err != nil {
				log.Println("read:", err)
				return
			}

			log.Printf("recv: %+v", post)
		}
	}()

	for {
		select {
		case <-interrupt:
			log.Println("interrupt")

			// Cleanly close the connection by sending a close message and then
			// waiting (with timeout) for the server to close the connection.
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				log.Println("write close:", err)
				return
			}

			return
		}
	}
}

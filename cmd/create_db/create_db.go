package main

import (
	"bytes"
	"log"
	"os/exec"
)

func main() {
	cmd := exec.Command("createdb", "-p", "5432", "-h", "localhost", "-e", "lifeatx_dev")

	var out bytes.Buffer
	cmd.Stdout = &out

	if err := cmd.Run(); err != nil {
		log.Printf("Error: %v", err)
	}

	log.Printf("Output: %q\n", out.String())
}

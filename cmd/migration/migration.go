package main

import (
	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gopkg.in/gormigrate.v1"
)

func main() {
	dbURL := os.Getenv("DATABASE_URL")
	if dbURL == "" {
		dbURL = "host=localhost port=5432 user=postgres dbname=lifeatx_dev sslmode=disable"
	}

	db, err := gorm.Open("postgres", dbURL)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	db.LogMode(true)

	m := gormigrate.New(db, gormigrate.DefaultOptions, []*gormigrate.Migration{
		{
			ID: "201910111",
			Migrate: func(tx *gorm.DB) error {
				type Post struct {
					gorm.Model
					Content   string `gorm:"not null"`
					ImagePath string `gorm:"not null"`
				}

				return tx.AutoMigrate(&Post{}).Error
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.DropTable("posts").Error
			},
		},
		{
			ID: "201910121",
			Migrate: func(tx *gorm.DB) error {
				type User struct {
					gorm.Model
					Sub   string `gorm:"not null"`
					Email string `gorm:"not null"`
				}

				return tx.AutoMigrate(&User{}).Error
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.DropTable("users").Error
			},
		},
		{
			ID: "201910122",
			Migrate: func(tx *gorm.DB) error {
				type User struct {
					Sub   string `gorm:"not null"`
					Email string `gorm:"not null"`
				}

				return tx.Model(&User{}).AddUniqueIndex("idx_user_email", "email", "sub").Error
			},
			Rollback: func(tx *gorm.DB) error {
				return nil
			},
		},
		{
			ID: "201910221",
			Migrate: func(tx *gorm.DB) error {
				type Post struct {
					UserID uint
				}

				type User struct {
					Posts []Post
				}

				return tx.AutoMigrate(&User{}).AutoMigrate(&Post{}).Error
			},
			Rollback: func(tx *gorm.DB) error {
				return nil
			},
		},
		{
			ID: "201910222",
			Migrate: func(tx *gorm.DB) error {
				type Post struct{}

				return tx.Model(&Post{}).AddForeignKey("user_id", "users(id)", "RESTRICT", "RESTRICT").Error
			},
			Rollback: func(tx *gorm.DB) error {
				type Post struct{}

				return tx.Model(&Post{}).RemoveForeignKey("user_id", "users(id)").Error
			},
		},
	})

	if err = m.Migrate(); err != nil {
		log.Fatalf("Could not migrate: %v", err)
	}

	log.Printf("Migration did run successfully")
}

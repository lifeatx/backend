module backend

require (
	github.com/auth0/go-jwt-middleware v0.0.0-20190805220309-36081240882b
	github.com/aws/aws-sdk-go v1.25.11
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/facebookgo/clock v0.0.0-20150410010913-600d898af40a // indirect
	github.com/facebookgo/grace v0.0.0-20180706040059-75cf19382434
	github.com/facebookgo/httpdown v0.0.0-20180706035922-5979d39b15c2 // indirect
	github.com/facebookgo/stats v0.0.0-20151006221625-1b76add642e4 // indirect
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/gorilla/websocket v1.4.1
	github.com/imroc/req v0.2.4
	github.com/jinzhu/gorm v1.9.11
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/lib/pq v1.2.0 // indirect
	github.com/markbates/grift v1.1.0 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337 // indirect
	github.com/urfave/negroni v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20190820162420-60c769a6c586 // indirect
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/gormigrate.v1 v1.6.0
)

go 1.13

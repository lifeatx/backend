package main

import (
	"backend/models"
	"bytes"
	"fmt"
	"mime/multipart"
	"net/http"
	"net/url"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/google/uuid"
	"github.com/labstack/echo"
)

const (
	S3_BUCKET = "lifeatx-images"
)

type PostHandlers struct {
	s3Session *session.Session
	broadcast chan models.Post
}

func (p *PostHandlers) GetPosts(c echo.Context) error {
	var posts []models.Post

	if err := DB.gorm.Find(&posts).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, &posts)
}

func (p *PostHandlers) GetUserPosts(c echo.Context) error {
	var posts []models.Post

	user := c.Get("User").(*models.User)

	if err := DB.gorm.Model(&user).Related(&posts).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, &posts)
}

func (p *PostHandlers) UploadPosts(c echo.Context) error {
	content := c.FormValue("content")

	file, err := c.FormFile("file")
	if err != nil {
		return err
	}

	user := c.Get("User").(*models.User)

	filePath, err := p.uploadToS3(file)

	newPost := models.Post{
		Content:   content,
		ImagePath: filePath,
		UserID:    user.ID,
	}

	if err := DB.gorm.Create(&newPost).Error; err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err.Error())
	}

	// Broadcast new post to all clients
	p.broadcast <- newPost

	return c.JSON(http.StatusCreated, &newPost)
}

func (p *PostHandlers) uploadToS3(file *multipart.FileHeader) (path string, err error) {
	src, err := file.Open()
	if err != nil {
		return "", err
	}
	defer src.Close()

	// Get file size and read the file content into a buffer
	var size int64 = file.Size
	buffer := make([]byte, size)
	src.Read(buffer)

	id := uuid.New()
	fileDir := fmt.Sprintf("/uploads/%s-%s", id.String(), url.QueryEscape(file.Filename))

	_, err = s3.New(p.s3Session).PutObject(&s3.PutObjectInput{
		Bucket:               aws.String(S3_BUCKET),
		Key:                  aws.String(fileDir),
		ACL:                  aws.String("public-read"),
		Body:                 bytes.NewReader(buffer),
		ContentLength:        aws.Int64(size),
		ContentType:          aws.String(http.DetectContentType(buffer)),
		ContentDisposition:   aws.String("attachment"),
		ServerSideEncryption: aws.String("AES256"),
	})

	if err != nil {
		return "", err
	}

	return fileDir, nil
}
